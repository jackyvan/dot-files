syntax on
set number
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4
set autoindent
set cindent
set ruler

" backspace over everything in insert mode
set backspace=indent,eol,start

" Automatically open NERDTree when vim starts up & pipe into `wincmd w` to
" switch focus
autocmd vimenter * NERDTree | wincmd w
nmap <C-b> :NERDTreeToggle<CR>
" noremap <C-w>- :split<CR>
" noremap <C-w>\| :vsplit<CR>
nmap \p :ProseMode<CR>

" Automatically open NERDTree when vim starts up and no files specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" close vim if only window left open is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" NERDTree show hidden files
let NERDTreeShowHidden=1

filetype plugin indent on
filetype on
filetype indent on

" Ruby / Rails
autocmd FileType ruby setlocal expandtab shiftwidth=2 tabstop=2

" fzf
set rtp+=/usr/local/opt/fzf

" Custom mode for distraction-free editing
function! ProseMode()
  call goyo#execute(0, [])
  set spell noci nosi noai nolist noshowmode noshowcmd
  set complete+=s
  set bg=light
  colors solarized
endfunction
command! ProseMode call ProseMode()
command! Focus call ProseMode()
